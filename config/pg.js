module.exports = {
    "user": process.env.PG_ENV_POSTGRES_USER || "postgres",
    "host": process.env.PG_PORT_5432_TCP_ADDR || "localhost",
    "database": process.env.PG_ENV_POSTGRES_DB || "test_easypay",
    "password": process.env.PG_ENV_POSTGRES_PASSWORD || "admin",
    "port": process.env.PG_PORT_5432_TCP_PORT || 5432
};