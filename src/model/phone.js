let db = require('../db');
module.exports = {add, remove, list, check};

function add(phone, cb) {
    db.query(`INSERT INTO phones (phone) VALUES ($1) ON CONFLICT DO NOTHING;`, [phone], (err, data) => {
        let result = data && data.rowCount;
        cb(err, result);
    });
}

function remove(phone, cb) {
    db.query(`DELETE FROM public.phones WHERE phone = $1;`, [phone], (err, data) => {
        let result = data && data.rowCount;
        cb(err, result);
    });
}

function check(phone, cb) {
    db.query(`SELECT 1 from phones WHERE phone = $1`, [phone], (err, data) => {
        let result = data && data.rowCount > 0;
        cb(err, result);
    });
}

function list(cb) {
    db.query(`SELECT phone from phones;`, [], (err, data) => {
        let result = data && data.rows.map(e => e.phone) || [];
        cb(err, result);
    });
}

