module.exports = {validatePhone, normalizePhone};

function validatePhone(p) {
    if (!p) return 1;
    if (/[^\d-+() ]/.test(p)) return 2; //посторонние символы
    if (!/^(\D*\d\D*){2,11}$/.test(p)) return 3; //некорректное количество цифр
    return 0;
}

function normalizePhone(p) {
    return p.replace(/[-() ]|(?=^\+[^+]*)|\+/gmi, '');//чистим вспомогательные символы и повторные плюсы
}