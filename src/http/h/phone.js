let model = require('../../model/phone');
let validators = require('./validators');
module.exports = {phone_add, phone_remove, phone_check, phone_list};

function param_phone(req) {
    let phone = req.query.phone || '';
    if (!phone)
        throw {statusCode: 400, message: "argument phone is missed"};
    if (validators.validatePhone(phone))
        throw {statusCode: 400, message: "phone is invalid"};
    return validators.normalizePhone(phone);
}
function phone_add(req, res, next) {
    let phone;
    try {
        phone = param_phone(req);
    } catch (err) {
        return next(err);
    }

    model.add(phone, (err, data) => {
        if (data === undefined) return next({statusCode: 500});
        else if (data === 0) return res.status(200).send('already exists');
        else return res.status(200).send('added');
    });
}

function phone_remove(req, res, next) {
    let phone;
    try {
        phone = param_phone(req);
    } catch (err) {
        return next(err);
    }

    model.remove(phone, (err, data) => {
        if (data === undefined) return next({statusCode: 500});
        else if (data === 0) return res.status(200).send('not exists');
        else return res.status(200).send('removed');
    });
}

function phone_check(req, res, next) {
    let phone;
    try {
        phone = param_phone(req);
    } catch (err) {
        return next(err);
    }

    model.check(phone, (err, data) => {
        if (data === undefined) return next({statusCode: 500});
        else if (!data) return res.status(200).send('not exists');
        else return res.status(200).send('exist');
    });
}

function phone_list(req, res, next) {
    model.list((err, data) => {
        if (data === undefined) return next({statusCode: 500});
        else return res.status(200).send(data);
    });
}