let passport = require('passport'),
    model = require('../../model/user');
module.exports = {account_login, account_verify, account_logout};


function account_login(req, res, next) {
    passport.authenticate('github', {
        successRedirect: req.cookies.askUrl || '/',
        failureRedirect: '/login'
    })(req, res, next);
}
function account_verify(req, res, next) {
    passport.authenticate('github', {
        successRedirect: req.cookies.askUrl || '/',
        failureRedirect: '/login'
    })(req, res, next);
}
function account_logout(req, res, next) {
    req.logout();
    res.redirect('/');
}