let passport = require('passport'),
    GitHubStrategy = require('passport-github').Strategy,
    express = require('express'),
    router = express.Router(),
    config = require('config/oauth'),
    session = require('express-session'),
    NedbStore = require('connect-nedb-session')(session);
module.exports = router;

/* passport */
passport.use(new GitHubStrategy({
        clientID: config.clientID,
        clientSecret: config.clientSecret,
        callbackURL: config.callbackURL
    },
    function (accessToken, refreshToken, profile, cb) {
        //здесь можно бы проверить пароли или раздать роли, но у нас все просто, и их нет
        //и поэтому любой валидный пользователь гитхаба будет валиден и у нас
        console.log('\033[0;33m', `[SECURITY] login ${profile.username}`, '\033[0m');
        cb(null, {
            githubId: profile.id,
            username: profile.username,
            token: accessToken
        });
    }
));

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

/* router */
router.use(session({
    secret: 'keyboard cat)',
    resave: true,
    saveUninitialized: true,
    cookie: {maxAge: 24 * 3600 * 1000},
    store: new NedbStore({filename: 'session.nedb'})
}));

router.use(passport.initialize());
router.use(passport.session());