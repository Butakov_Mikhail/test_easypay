let express = require('express'),
    cookieParser = require('cookie-parser'),
    path = require('path');

let app = express(),
    router = require('./router');

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "..", "public")));

app.set('views', path.join(__dirname, 't'));
app.set('view engine', 'ejs');
app.use(router);

app.listen(80, function () {
    console.log('Express server listening on port 80');
});
