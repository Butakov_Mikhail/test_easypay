let express = require('express'),
    router = express.Router(),
    oauth = require('./oauth'),
    {checkAuth, sendError, sendPage, notFound, logError, logRequest} = require('./util'),
    {account_login, account_verify, account_logout} = require('./h/account'),
    {phone_add, phone_remove, phone_check, phone_list} = require('./h/phone');
module.exports = router;

/************************************* http интерфейс ***********************************/

router.use(logRequest);

/* вся авторизация как единый middleware*/
router.use(oauth);

/* Сервис аккаунтов */
router.get('/api/account/login', [account_login]);
router.get('/api/account/verify', [account_verify]);
router.get('/api/account/logout', [account_logout]);
router.get('/api/account/login', [checkAuth, account_login]);
router.get('/api/account/logout', [checkAuth, account_logout]);

/* Сервис работы с телефонами */
router.get('/api/phone/add', [checkAuth, phone_add]);
router.get('/api/phone/remove', [checkAuth, phone_remove]);
router.get('/api/phone/check', [checkAuth, phone_check]);
router.get('/api/phone/list', [checkAuth, phone_list]);

/* Веб-интерфейс */
router.get('/login', [sendPage]);
router.get('/logout', [sendPage]);
router.get('/list', [checkAuth, sendPage]);
router.get('/edit', [checkAuth, sendPage]);
router.get('/', [checkAuth, sendPage]);
router.get('*', [notFound]);


router.use(logError);
router.use(sendError);