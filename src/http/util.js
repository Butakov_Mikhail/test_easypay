module.exports = {
    checkAuth,
    sendError, sendPage, notFound,
    logError, logRequest
};

/* плюшки для красивых логов*/
const logtime = () => new Date().toJSON().replace(/[-.:]|\d\d\dZ/g, '').replace(/T/, '_');

function logRequest(req, res, next) {
    console.log('\033[0;94m', `[ROUTER] ${logtime()} ${req.method} ${req.url}`, '\033[0m');
    next();
}
function logError(err, req, res, next) {
    let username = req.user && req.user.username || 'Аноним';
    console.error('\033[0;91m', `[ERROR]  ${logtime()} ${req.method} ${req.url} [${username}] - ${err.statusCode}: ${err.message}`, '\033[0m');
    next(err);
}
/*шлет неавторизованных на авторизацию*/
function checkAuth(req, res, next) {
    next(req.isAuthenticated() ? null : {statusCode: 401, message: 'Необходима авторизация', askUrl: req.url});
}
/*отдача ошибок*/
function sendError(err, req, res, next) {
    let isApi = /^\/api/i.test(req.url);

    res.status(err.statusCode || 500);

    if (!isApi && err.statusCode === 401) {
        res.cookie('askUrl', err.askUrl || '/');
        res.redirect('/login');
    } else if (!isApi && err.statusCode === 404) {
        res.render('e404', {message: err.message});
    } else if (isApi) {
        res.send(err.message);
    } else {
        res.render('e500', {message: err.message});
    }
}
/*отдача страниц*/
function sendPage(req, res, next) {
    let username = req.user && req.user.username || "Анонимус";
    res.render('page', {username});
}

/*заглушка для ненайденных страниц*/
function notFound(req, res, next) {
    next({statusCode: 404});
}