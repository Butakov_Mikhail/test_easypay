CREATE TABLE phones
(
    phone VARCHAR(16)
);
CREATE UNIQUE INDEX phones_phone_uindex ON phones (phone);