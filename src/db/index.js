const {Pool} = require('pg'),
    config = require('config/pg');
module.exports = {query};

const pool = new Pool({
    user: config.user,
    host: config.host,
    database: config.database,
    password: config.password,
    port: config.port
});

pool.on('error', (err, client) => console.error('\033[0;35m', `[DB ERROR]  ${err}`, '\033[0m'));

function query(q, v, cb) {
    console.log('\033[0;35m', `[DB QUERY] ${q}`, '\033[0m');
    return pool.query(q, v, (err, data) => {
        if (err) console.error('\033[7;35m', `[DB ERROR] ${err.message}\n\t${q}\n\t${v}`, '\033[0m');
        cb(err, data);
    });
}