import Vue from "vue";
import VueRouter from "vue-router";
import vues from "./components";

/*регистрация окмпонентов*/
let components = vues.map(({tag, impl}) => {
    Vue.component(tag, impl);
    return tag;
});
console.log('Зарегистрированы компоненты', components);
/*регистрация роутов*/
Vue.use(VueRouter);
let routes = vues.filter(e => e.path).map(({path, impl}) => ({path, component: impl}));
const router = new VueRouter({
    mode: 'history',
    routes: routes
});

/* регистрация директив */
Vue.directive('title', (el, binding) => document.title = binding.value);
Vue.directive('allow', (el, binding) => {
    if (binding.value) el.removeAttribute('disabled');
    else el.setAttribute('disabled', '1');
});

HTMLInputElement.prototype.isValid = function () {
    return this.matches(':valid');
}
/*регистрация корня*/
let app = vues.find(e => e.tag === 'app').impl;
new Vue({
    render: h => h(app),
    router: router
}).$mount('#app');
