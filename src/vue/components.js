export default [
    {tag: 'app', impl: require('./page/app.vue')},
    {tag: 'page-index', impl: require('./page/index.vue'), path: '/'},
    {tag: 'page-login', impl: require('./page/login.vue'), path: '/login'},
    {tag: 'page-edit', impl: require('./page/edit.vue'), path: '/edit'},
    {tag: 'page-list', impl: require('./page/list.vue'), path: '/list'},
    {tag: 'input-phone', impl: require('./part/input-phone.vue')},
    {tag: 'list-phone', impl: require('./part/list-phone.vue')},
    {tag: 'form-add-phone', impl: require('./part/form-add-phone.vue')},
    {tag: 'form-del-phone', impl: require('./part/form-del-phone.vue')},
    {tag: 'form-check-phone', impl: require('./part/form-check-phone.vue')},
    {tag: 'nav-menu', impl: require('./part/nav-menu.vue')}
];