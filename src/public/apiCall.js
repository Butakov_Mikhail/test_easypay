const apiGet = (url) => console.log(`%c[API CALL] ${url}`, 'color:skyblue') || fetch(url, {credentials: 'include'});
const logResult = (r) => console.log(`%c[API RESULT]`, 'color:skyblue', r) || r;
window.api = {
    phone_add: (phone) => apiGet(`/api/phone/add?phone=${phone}`)
        .then(r => r.text())
        .then(logResult),
    phone_del: (phone) => apiGet(`/api/phone/remove?phone=${phone}`)
        .then(r => r.text())
        .then(logResult),
    phone_check: (phone) => apiGet(`/api/phone/check?phone=${phone}`)
        .then(r => r.text())
        .then(logResult),
    phone_list: (phone) => apiGet(`/api/phone/list`)
        .then(r => r.json())
        .then(logResult)
};