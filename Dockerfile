FROM node:carbon
WORKDIR /home/app

RUN apt-get update
RUN yes | apt-get install postgresql-client nano
COPY . .
RUN dir .
RUN npm i
EXPOSE 80

CMD npm start